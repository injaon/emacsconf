;-*-mode:Emacs-Lisp;-*-

(add-to-list 'load-path "/home/injaon/.emacs.d/libs/")
(add-to-list 'load-path "/home/injaon/.emacs.d/libs/python.el/python-mode.el")
(add-to-list 'load-path "/home/injaon/.emacs.d/libs/coffee-mode.el/")
(add-to-list 'load-path "/home/injaon/.emacs.d/libs/templates/")
(add-to-list 'load-path "/home/injaon/.emacs.d/libs/js2/")
(require 'python)
(require 'coffee-mode)
(require 'json-mode)

;; Template Mode
(require 'template)
(template-initialize)

;; sql-indent
(eval-after-load "sql"
  (load-library "sql-indent"))

;; php
(autoload 'php-mode "php-mode" "Major mode for editing php code." t)
(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.inc$" . php-mode))
(add-hook 'php-mode-hook (electric-indent-mode t))
;;(require 'php-completion)

;; Autocomplete-mode
(add-to-list 'load-path "/home/injaon/.emacs.d/libs/ac-mode/")
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "/home/injaon/.emacs.d/libs/ac-mode//ac-dict")
(ac-config-default)
(require 'ac-python)

(autoload 'js2-mode "js2-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))

;;;;;;;;;;;;;;;;;;;;
;; Cool Minor-Modes
;;;;;;;;;;;;;;;;;;;;
(column-number-mode    t)
(size-indication-mode  t)
;; overwrite the region
(delete-selection-mode t)
;; highligth the region
(transient-mark-mode   t)
;; no menubar & toolbar
(menu-bar-mode -1)
;; highlight the open/close paren
(show-paren-mode t)
;; change windows with S-arrows
(windmove-default-keybindings)
;; ALWAYS linum-mode
(global-linum-mode t)
(setq linum-format "%d| ")
;; ALWAYS White-space mode
(setq whitespace-style '(face empty tabs lines-tail trailing))
(global-whitespace-mode t)

;;;;;;;;;;;;;;;;;;;;
;; Global Config
;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(current-language-environment "UTF-8")
 '(tool-bar-mode t))
(setq bookmark-save-flag 1)
;; allow scroll while searching
(setq isearch-allow-scroll t)
;; ask for a final newline
(setq require-final-newline "visit-save")
(setq confirm-nonexistent-file-or-buffer nil)
;; default mode: text-mode
(setq default-major-mode 'text-mode)
;; no startup msg
(setq-default inhibit-startup-message t)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-comment-face ((t (:foreground "Red"))))
 '(mode-line ((t (:bold t :foreground "White" :background "Black")))))
(defalias 'yes-or-no-p 'y-or-n-p)
; allow up/down case region
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; General hooks
(add-hook 'before-save-hook 'delete-trailing-whitespace)
;; Tabs are evil
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq indent-line-function 'insert-tab)


;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Case Convertion
(defun upcase-word-my ()
  "upcase a whole word"
  (interactive)
  (upcase-word 1)
  (upcase-word -1))

(defun downcase-word-my ()
  "downcase a word"
  (interactive)
  (downcase-word 1)
  (downcase-word -1))

; Capitalize
(defun capitalize-word-my ()
  "upcase the first letter of the word"
  (interactive)
  (backward-word)
  (capitalize-word 1))

; Killing
(defun kill-word-my ()
  "Kill a whole word"
  (interactive)
  (backward-word)
  (kill-word 1))

(defadvice kill-ring-save (before slick-copy activate compile)
  "When calle interactively with no active region, copy a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end)) (message "Copied line")
     (list (line-beginning-position) (line-beginning-position 2)))))

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(defadvice goto-line (after expand-after-goto-line
                            activate compile)
  "hideshow-expand affected block when using goto-line in a collapsed buffer"
  (save-excursion
    (hs-show-block)))

(defun comment-dwim-line (&optional arg)
  "Replacement for the comment-dwim command.
   If no region is selected and current line is not blank and we are not at
   the end of the line, then comment current line. Replaces default
   behaviour of comment-dwim, when it inserts comment at the end of the line."
  (interactive "*P")
  (comment-normalize-vars)
  (if (and (not (region-active-p)) (not (looking-at "[ \t]*$")))
      (comment-or-uncomment-region (line-beginning-position) (line-end-position))
    (comment-dwim arg)))

;; Inserts
(defun insert-gpl2-license ()
  (interactive)
  (insert "/*\n"
          " * Copyright (C) 2010 injaon\n"
          " *\n"
          " * This program is free software; you can redistribute it and/or modify\n"
          " * it under the terms of the GNU General Public License as published by\n"
          " * the Free Software Foundation; either version 2 of the License, or\n"
          " * (at your option) any later version.\n"
          " *\n"
          " * This program is distributed in the hope that it will be useful,\n"
          " * but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
          " * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
          " * GNU General Public License for more details.\n"
          " *\n"
          " * You should have received a copy of the GNU General Public License along\n"
          " * with this program; if not, write to the Free Software Foundation, Inc.,\n"
          " * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.\n"
          " */\n\n"))

(defun djcb-duplicate-line (&optional commentfirst)
  "comment line at point; if COMMENTFIRST is non-nil, comment the original"
  (interactive)
  (beginning-of-line)
  (let
      ((beg (point)))
    (end-of-line)
    (let ((str (buffer-substring beg (point))))
      (when commentfirst
        (comment-region beg (point)))
      (insert-string
       (concat (if (= 0 (forward-line 1)) "" "\n") str "\n"))
      (forward-line -1))))

;;;;;;;;;;;;;;;;;;;;
;; My key Bindings
;;;;;;;;;;;;;;;;;;;;
;; unset some key-binding
(global-unset-key (kbd "C-f"))
(global-unset-key (kbd "C-p"))
(global-unset-key (kbd "C-n"))
;; (global-unset-key (kbd "C-o"))
(global-unset-key (kbd "C-b"))
(global-unset-key (kbd "C-x u"))
;; (global-unset-key (kbd "C-h"))
(global-unset-key (kbd "C-r"))       ; search-backeard
(global-unset-key (kbd "C-<end>"))   ; end-of-buffer
(global-unset-key (kbd "C-<home>"))  ; beginning-of-buffer
(global-unset-key (kbd "C-<prior>")) ; scroll-right
(global-unset-key (kbd "C-<next>"))  ; scroll-left
(global-unset-key (kbd "M-a"))       ; backward-sentence
(global-unset-key (kbd "M-e"))       ; foreward-sentence
(global-unset-key (kbd "M-m"))
(global-unset-key (kbd "M-d"))
(global-unset-key (kbd "M-k"))       ; delete-sentence

;; comments
(global-set-key "\M-;" 'comment-dwim-line)

;; Small Scrolling
(global-set-key (kbd "M-n") '(lambda () (interactive) (scroll-down 1)))
(global-set-key (kbd "C-n") '(lambda () (interactive) (scroll-up 1)))

; Duplicate Lines
(global-set-key (kbd "C-c <up>")
                (lambda () (interactive) (djcb-duplicate-line t)))
(global-set-key (kbd "C-c <down>")
                (lambda () (interactive) (djcb-duplicate-line)))

;; (global-set-key (kbd "C-d") 'backward-kill-word)
(global-set-key (kbd "C-f") 'forward-sexp)
(global-set-key (kbd "C-b") 'backward-sexp)
(global-set-key (kbd "M-k") 'kill-sexp) ;; kill text beetwen parentesis
(global-set-key (kbd "M-a") 'back-to-indentation)
(global-set-key (kbd "M-d") 'kill-word-my)

;; HideShow
(global-set-key (kbd "C-p h") 'hs-hide-all)
(global-set-key (kbd "C-p s") 'hs-show-all)
(global-set-key (kbd "C-p p") 'hs-toggle-hiding)

;; Replace
(define-key global-map (kbd "C-r") 'query-replace)

;; Subtitution
;; M-u
(substitute-key-definition
 'upcase-word 'upcase-word-my (current-global-map))
;; M-l
(substitute-key-definition
 'downcase-word 'downcase-word-my (current-global-map))
;; M-c
(substitute-key-definition
 'capitalize-word 'capitalize-word-my (current-global-map))

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extensions
;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Autopair mode
(autoload 'autopair-global-mode "autopair" nil t)
(autopair-global-mode)
(add-hook 'lisp-mode-hook 'autopair-mode)
;; abbrev-mode
(setq abbrev-file-name             ;; tell emacs where to read abbrev-file-name
      "~/.emacs.d/abbrev_defs")
(setq save-abbrevs t)
(abbrev-mode t)

;;;;;;;;;;;;;;;;;;;;
;; Others
;;;;;;;;;;;;;;;;;;;;
; hs-minor-mode on programming modes
(add-hook 'python-mode-hook 'hs-minor-mode)
(add-hook 'c-mode-hook 'hs-minor-mode)
(add-hook 'emacs-lisp-mode-hook 'hs-minor-mode)
(add-hook 'js2-mode-hook 'hs-minor-mode)
(add-hook 'coffee-mode-hook 'hs-minor-mode)

;; coffee-mode
;; (add-hook 'coffee-mode-hook 'auto-complete-mode-major-mode)
(add-to-list 'auto-mode-alist '("\\.coffee$" . coffee-mode))
(setq tab-width 4)

;; Web-Mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.ctp$" . web-mode))
(set-face-attribute 'web-mode-html-tag-face nil
                    :foreground "lightblue"
                    :weight 'bold)
(set-face-attribute 'web-mode-html-attr-name-face nil
                    :foreground "orange")
(set-face-attribute 'web-mode-doctype-face nil
                    :foreground "lightblue"
                    :slant 'italic)

(defun web-mode-hook ()
  "Hooks for Web mode."
  (global-whitespace-mode -1)
  (autopair-global-mode -1)
  )
(add-hook 'web-mode-hook 'web-mode-hook)
